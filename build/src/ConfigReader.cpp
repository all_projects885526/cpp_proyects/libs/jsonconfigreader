#include "RSJparser.tcc"


RSJresource readJsonConfig(std::string filename)
{
    constexpr const char * filepath = "out/config/";

    std::ifstream my_fstream (filepath + filename);
    return (RSJresource(my_fstream));
}
