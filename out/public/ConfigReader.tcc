#include "RSJparser.tcc"


template <class T>
T getFromJsonConfig(RSJresource &config, const char *attr) {
    return config[attr].as<T>();
}